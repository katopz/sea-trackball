﻿using UnityEngine;
using System.Collections;

public class MouseRollSphere : MonoBehaviour
{
    public float horizontalSpeed = 5f;
    public float verticalSpeed = 5f;

    // limit
    private float _MAX_MAQNITUDE = 500f;
    private float _MAX_VELOCITY = 15;
    private float _MIN_X = -18;
    private float _MAX_X = 18;
    private float _MAX_Y = 20;

    // jump
    public float jumpForce = 300f;
    private float hPower = 0f;
    private float vPower = 0f;
    
    private Rigidbody _rock { get { return GetComponent<Rigidbody>(); }}

    // Use this for initialization
    void Start()
    {
        Screen.SetResolution(1080, 1080, true);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // target
        if (!_rock)
        {
            return;
        }

        // world force

        //Debug.Log("sqrMagnitude:" + _rock.velocity.sqrMagnitude);
        //Debug.Log("velocity.x:" + _rock.velocity.x);
        //Debug.Log("x:" + _rock.transform.position.x);

        //_rock.AddForce(Vector3.down * 2 * _rock.mass);

        // MOUSE ///////////////////////////////////////////////////

        // direction
        var mH = Input.GetAxis("Mouse X");
        var mV = Input.GetAxis("Mouse Y");

        // normal force -----------------------------------

        var h = horizontalSpeed * mH;
        var v = verticalSpeed * mV;

        h /= (_rock.transform.position.y * .75f);
        v /= (_rock.transform.position.y * .75f);

        _rock.AddForce(h * Config.FORCE_FACTOR, 0, v * Config.FORCE_FACTOR);

        // jump force -----------------------------------

        if (_rock.transform.position.y > 3.0 && _rock.transform.position.y < 3.5)
        {
            hPower += Mathf.Abs(mH);
            vPower += Mathf.Abs(mV);

            if (hPower > 20 || vPower > 20)
            {
                _rock.AddForce(0, jumpForce * Config.FORCE_FACTOR, 0);
                hPower = vPower = 0;
            }
        }

        // lost power
        hPower /= 2;
        vPower /= 2;

        // KEY ///////////////////////////////////////////////////

        // exit
        if (Input.GetKeyDown("escape"))
        {
            Application.Quit();
        }

        // Speed limit
        if (_rock.velocity.sqrMagnitude > _MAX_MAQNITUDE)
        {
            //smoothness of the slowdown is controlled by the 0.99f, 
            //0.5f is less smooth, 0.9999f is more smooth
            _rock.velocity *= 0.99f;
        }

        // Overflow
        if (_rock.transform.position.y > _MAX_Y)
        {
            if (Mathf.Abs(_rock.velocity.x) > _MAX_VELOCITY)
            {
                _rock.velocity = new Vector3(_MAX_VELOCITY * Mathf.Abs(_rock.velocity.x) / _rock.velocity.x, _rock.velocity.y, _rock.velocity.z);
            }

            if (Mathf.Abs(_rock.velocity.y) > _MAX_VELOCITY)
            {
                _rock.velocity = new Vector3(_rock.velocity.x, _MAX_VELOCITY * Mathf.Abs(_rock.velocity.y) / _rock.velocity.y, _rock.velocity.z);
            }

            if (Mathf.Abs(_rock.velocity.z) > _MAX_VELOCITY)
            {
                _rock.velocity = new Vector3(_rock.velocity.x, _rock.velocity.y, _MAX_VELOCITY * Mathf.Abs(_rock.velocity.z) / _rock.velocity.z);
            }
        }

        // Left, try to go left
        if (_rock.transform.position.x < _MIN_X)
        {
            float r = Mathf.Abs(_rock.transform.position.x)/ _MAX_X;
            float way = Mathf.Abs(_rock.velocity.x) / _rock.velocity.x;
            float vx = _rock.velocity.x * 0.82f;

            // push right
            if (way < 0 && _rock.transform.position.x < _MAX_X * way)
            {
                Debug.Log("push >>>");
                _rock.velocity = new Vector3(vx,  _rock.velocity.y, _rock.velocity.z * 0.5f);
            }
        }

        // Right, try to go right
        if (_rock.transform.position.x > _MAX_X)
        {
            float r = Mathf.Abs(_rock.transform.position.x) / _MAX_X;
            float way = Mathf.Abs(_rock.velocity.x) / _rock.velocity.x;
            float vx = _rock.velocity.x * 0.97f;

            // push left
            if (way > 0 && _rock.transform.position.x > _MAX_X * way)
            {
                Debug.Log("push <<<");
                _rock.velocity = new Vector3(vx, _rock.velocity.y, _rock.velocity.z * 0.5f);
            }
        }
    }
}
