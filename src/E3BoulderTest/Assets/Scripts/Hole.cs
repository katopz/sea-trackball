﻿using UnityEngine;
using System.Collections;

public class Hole : MonoBehaviour
{

    public float delaySecond = 3f;
    public Vector3 shootForce = new Vector3(500, 1000, 0);
    public GameObject explosionParticle;
    public GameObject idleParticle;

    private float _stayTime = 0f;
    private Rigidbody _Rigidbody = null;

    private float _emissionRate = 0;
    private GameObject _explosionParticle;

    private bool _isExploding = false;

    // Use this for initialization
    void Start()
    {

    }

    void Update()
    {
        if (!_isExploding)
            return;

        if (_stayTime > 0)
        {
            _stayTime--;

            if (_Rigidbody)
            {
                _Rigidbody.AddForce(Config.EXPLODE_FORCE);
            }
        }
        else
        {
            GameObject.Destroy(_explosionParticle);
            _explosionParticle = null;
            _isExploding = false;
        }
    }

    // shoot force -----------------------------------

    void OnTriggerEnter(Collider other)
    {
        ParticleSystem _ParticleSystem = idleParticle.GetComponent<ParticleSystem>();
        _emissionRate = _ParticleSystem.emissionRate;
        _ParticleSystem.emissionRate = 0;
    }

    void OnTriggerExit(Collider other)
    {
        ParticleSystem _ParticleSystem = idleParticle.GetComponent<ParticleSystem>();
        _ParticleSystem.emissionRate = _emissionRate;
    }

    // Applies an upwards force to all rigidbodies that enter the trigger.
    void OnTriggerStay(Collider other)
    {
        _Rigidbody = other.attachedRigidbody;

        if (!_Rigidbody)
            return;

        // Once
        if (_isExploding)
            return;

        //Debug.Log("OnTriggerStay:" + _stayTime);
        _stayTime++;
        if (_stayTime > 60 * delaySecond)
        {
            // explode state
            _isExploding = true;

            // Force
            _Rigidbody.AddForce(shootForce * Config.FORCE_FACTOR);
            _Rigidbody = null;

            // Effect
            _explosionParticle = (GameObject)Instantiate(explosionParticle, this.transform.position, this.transform.rotation);
        }
    }
}
