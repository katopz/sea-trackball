﻿using UnityEngine;
using System.Collections;

public class Config : MonoBehaviour
{
	public static float FORCE_FACTOR = 2;
    public static Vector3 EXPLODE_FORCE = new Vector3(6f,0,4f);
}
